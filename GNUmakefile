# makefile

#LOGO  = OGL-Logo.jpeg
LOGO  = d20.jpeg
TITLE = mythos-and-madness
VER   = $(shell date +%y.%m.%d)

build:
	mkdir $@

help:
	@echo "make html     Generate book as HTML."
	@echo "make txt      Generate book as plain text."
	@echo "make epub     Generate book as an epub."
	@echo "make pdf      Generate book as an PDF."
	@echo "make release  Build a release ZIP file."
	@echo "make clean    Remove temporary build files."
	@echo "make nuke     Remove temporary build and release files."

html:	build 
	@mkdir dist  || true
	xmlto --skip-validation -o build html-nochunks src/book.xml
	@mv build/tmp.html dist/$(TITLE).html

txt:	build 
	@mkdir dist  || true
	xmlto --skip-validation -o build txt src/book.xml
	@mv build/tmp.txt dist/$(TITLE).txt

epub:	build
	@mkdir build/epub || true
	xsltproc --output build/epub/ docbook/epub/docbook.xsl src/book.xml
	@cp img/$(LOGO) build/epub/OEBPS/$(LOGO)
	@cp img/cover_front.jpeg build/epub/OEBPS/cover.jpeg
	@cp font/*ttf build/epub/OEBPS/
	@cat style/style.css > build/epub/OEBPS/style.css 
	@sed -i 's_../img/__g' build/epub/OEBPS/index.html
	@sed -i 's_../img/__g' build/epub/OEBPS/content.opf
	@sed -i 's_jpeg" media-type=""/>_jpeg" media-type="image/jpeg"/>_g' build/epub/OEBPS/content.opf
	@sed -i 's_ncx"/>_ncx"/><item id="idm0" href="Andada-Regular.ttf" media-type="application/x-font-ttf"/><item id="idm2" href="cover.jpeg" media-type="image/jpeg"/><item id="idm3" href="junction-bold.ttf" media-type="application/x-font-ttf"/><item id="idm4" href="junction-light.ttf" media-type="application/x-font-ttf"/><item id="idm5" href="junction-regular.ttf" media-type="application/x-font-ttf"/><item id="idm6" href="style.css" media-type="text/css"/><item id="idm7" href="texgyrebonum-bold.ttf" media-type="application/x-font-ttf"/><item id="idm8" href="texgyrebonum-bolditalic.ttf" media-type="application/x-font-ttf"/><item id="idm9" href="texgyrebonum-italic.ttf" media-type="application/x-font-ttf"/><item id="idm10" href="texgyrebonum-regular.ttf" media-type="application/x-font-ttf"/>_' build/epub/OEBPS/content.opf
	@find build/epub/OEBPS/ -name "*html" -exec sed -i 's_<head>_<head>\n\n_' {} \;
	#@find build/epub/OEBPS/ -name "*html" -exec sed -i "/<head>/r style/style.css" {} \;
	@find build/epub/OEBPS/ -name "*html" -exec sed -i 's_</head>_<link rel="stylesheet" href="style.css" /></head>_' {} \;
	@mv build/epub/OEBPS .
	@mv build/epub/META-INF .
	@cat src/mimetype > mimetype
	@zip -X -0 $(TITLE).epub mimetype
	@rm mimetype
	@zip -X -9 $(TITLE).epub -r META-INF OEBPS
	@rm -rf META-INF OEBPS
	@mv $(TITLE).epub dist

pdf:	build
	@test -d build || mkdir build
	@mkdir dist  || true
	inkscape --export-png=build/OGL-Logo.png \
	--export-dpi=300 img/OGL-Logo.svg 
	xsltproc --output build/tmp.fo \
	--xinclude \
	--stringparam paper.type  A4 \
	--stringparam page.width 210mm \
	--stringparam page.height 292mm \
	--stringparam my.guild.logo "../img/OGL-Logo.jpeg" \
	--stringparam redist.text "ogl" \
	--stringparam column.count.titlepage 1 \
	--stringparam column.count.lot 1 \
	--stringparam column.count.front 1 \
	--stringparam column.count.body 2 \
	--stringparam column.count.back 1 \
	--stringparam column.count.index 2 \
	--stringparam body.font.family "TeX Gyre Bonum" \
	--stringparam title.font.family "Andada" \
	--stringparam bridgehead.font.family "Junction" \
	--stringparam symbol.font.family "UniCons" \
	--stringparam footer.column.widths "1 0 0" \
	--stringparam body.font.master 10 \
	--stringparam body.font.size 10 \
	--stringparam chapter.autolabel 0 \
	style/mystyle.xsl src/book.xml
	fop -c style/rego.xml build/tmp.fo build/tmp.pdf
	#ps2pdf character-sheets/character_mythos-and-madness+boring.ps \
	#build/character_mythos-and-madness+boring.pdf
	inkscape character-sheets/character_mythos-and-madness+fancy.svg \
	--export-pdf=build/character_mythos-and-madness+fancy.pdf
	inkscape img/cover.svg --export-pdf=build/cover.pdf 
	pdftk \
	build/cover.pdf \
	build/tmp.pdf \
	build/character_mythos-and-madness+fancy.pdf \
	cat output build/$(TITLE).pdf
	pdftk build/$(TITLE).pdf update_info src/bookmarks.txt \
	output dist/$(TITLE).pdf

release:
	@mkdir $(TITLE)-$(VER)
	@cp dist/$(TITLE).* \
	LICENSE README.md \
	$(TITLE)-$(VER)
	@zip $(TITLE)-$(VER).zip -r \
	$(TITLE)-$(VER)

clean:
	@rm -rf build
	@rm -f src/tmp*xml
	@rm -f *.tmp
	@rm -rf OEBPS
	@rm -rf META_INF

nuke:	clean
	@rm -rf dist

