# Mythos and madness

*Mythic thrills and modern intrigue*

An investigative horror roleplaying game based on D&D 3.5 (Pathfinder) rules and adapted from Gareth Hanrahan's **OGL Horror** game.
Because it's based on the 3.5 edition of D&D, you can use 3.5 NPC, monster, and even most item stats with little to no adaptation.

To keep this book minimal, rules that exist in [Pathfinder](https://paizo.com/products/btpy88yj?Pathfinder-Roleplaying-Game-Core-Rulebook) and [Pathfinder Unchained](https://paizo.com/products/btpy9c25?Pathfinder-Roleplaying-Game-Pathfinder-Unchained) have not been reproduced here.
To play this game, you should be familiar with D&D 3.5, Pathfinder, or Starfinder, and the d20 system in general.

## Elevator pitch

This is intended as a d20-based Lovecraft (or [Dunsany](https://gitlab.com/notklaatu/dpegana20), if you prefer) horror game, but is likely suitable for any modern story.
This book imposes no particular setting, although it does assume you're playing ordinary people in a relatively modern time period.
However, because it's based on 3.5 rules, you can use equipment appropriate to any setting, as found in any 3.5-compatible book. 
Yes, you can play a Mythos-inspired horror game in your own city in the modern day, Chicago in the 1920s, Casablanca in the 1940s, or even the Forgotten Realms or Golarion.

## How do I build this?

What you are looking at right now is the source code for the rules. If you want to download and play this game, it's much easier download a PDF that's already been generated.

### No really, I want to build this.

If, for whatever reason, you want to build this thing from source, you can do that. This was written on Slackware Linux and I've lazily structured it around Slackware's default Docbook install. If you are familiar with XML and Docbook, you should have no problem.

The build requirs:

* [Docbook](https://docbook.org)
* [xsltproc](https://xmlsoft.org/XSLT/index.html)
* [FOP 2](https://xmlgraphics.apache.org/fop/)
* [Inkscape](https://inkscape.org) for SVG conversion

These are the steps I would take to do the build, were I you:

1. Clone this repo
2. Verify the Docbook paths in the ``book.xml`` file are appropriate for your system.
3. Verify that you have these fonts: [DejaVu Sans](https://dejavu-fonts.github.io/) and [Junction](https://www.theleagueofmoveabletype.com/). These are the fonts specified by the [Dungeon Masters Guild](https://www.dmsguild.com/) templates from **Wizards of the Coast**, so they aren't literally required (edit ``GNUmakefile`` to change them), but they make it look nice.
4. ``make clean pdf``
5. Find the PDF in the ``dist`` directory.

## License

All text is licensed under the [Open Game License 1.0a (OGL)](http://www.opengamingfoundation.org/ogl.html).
