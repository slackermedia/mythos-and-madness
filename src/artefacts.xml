<chapter id="artefacts">
  <title>Artefacts</title>

  <para>
    Each artefact is unique, its provenance the subject of occult speculation and frenzied research, its fate the subject of endless and often lethal conspiracy.
    An artefact is unlikely to be found in the hands of only a single character.
    Most have whole organisations or cults dedicated to protecting or exploiting them.
  </para>

  <section id="artefacts-black-grail">
    <title>Black Grail</title>

    <para>
      The Black Grail is a stone bowl, octagonal in shape.
      It is carved from black stone, with striations of silver and green metal running through it.
      It was dredged up from the mid-Atlantic by a fishing vessel in the 1850s and sold to an antique dealer.
      It passed from private collection to private collection until it was eventually purchased by the British Museum in 1913.
      The ship carrying the Grail, the <emphasis>Mariposa</emphasis>, never arrived.
      Its wreck was found in the Arctic in 1957.
    </para>

    <para>
      According to records, water from the Black Grail had strange effects on anyone who drank it.
      One antiquarian in Boston has a stuffed animal which he claims is the preserved body of a deformed dog that drank from the Grail.
      Biologists claim the tentacles growing from the dog's mouth and face are just octopus or squid parts grafted on after death.
    </para>

    <para>
      Some theorise that the metal in the Grail was radioactive, causing water contained in it to have mutative side-effects.
      At least one vial of water from the Grail is kept in a private collection.
    </para>

    <para>
      Perhaps the Grail was a product of long-vanished Atlantis.
      Its transforming powers could be caused by magic, or advanced genetic-engineering technology from ages past.
      As to what happened to it: could the <emphasis>Mariposa</emphasis> have been intercepted and boarded by thieves?
      Or did the Grail's power corrupt the crew?
      Has the Grail returned to the ocean, or is it in the hands of some ancient cult?
      Or possibly worse still, a cutting-edge biogenetics company?
    </para>

    <para><emphasis role="bold">Abilities:</emphasis>
    The Grail has only one power: whosoever drinks from it starts to change.
    The changes are usually adaptive, so a drinker who falls into the ocean might begin to grow gills, while someone trapped in the darkness might begin to turn into a grotesque rat or bat hybrid.
    The period of change usually lasts no more than a day, although secondary transformations can be
triggered by stress in later months.
    </para>
  </section>
  
  <section id="artefacts-book-of-dead-names">
    <title>Book of Dead Names</title>

    <para>
      The <emphasis>Book of Dead Names</emphasis> is an encyclopaedia of occult lore, assembled from the writings of dozens of occultists and master sorcerers.
      Whenever one of these masters feels that his life is beginning to ebb away, he writes down the essence of all that he has learned and places it in the book.
      It is the secret heritage of the occult underworld, a supreme act of erudition and generosity to the next generation of sorcerers.
    </para>

    <para>
      Of course, master sorcerers are liars.
      While a few did pass on their lore freely, most encoded their personalities in graven rituals, or locked their spirits in paintings to be included with the book, or reduced themselves to essential salts and left instructions in the book on how they should be resurrected.
      Anyone reading the book is assailed by a clamouring horde of powerful ghosts, each one seeking a new host body.
    </para>

    <para>
      There is no shortage of candidates.
      The book is one of the most comprehensive books of lore in the world, and there are always those willing to risk their souls for knowledge.
      The keeper of the book changes with alarming regularity, as one occult group or another seizes
      control of it.
    </para>

    <para><emphasis role="bold">Abilities:</emphasis> The Book of Dead Names contains all manner of spells and rituals.
    It gives a +10 equipment bonus to any Research or Knowledge (occult) checks.
    However, opening the book requires a Will save (DC 18) to avoid possession by the spirit of some long-dead sorcerer.
    </para>
  </section>

  <section id="artefacts-coniferous-skull">
    <title>Coniferous skull</title>

    <para>
      An ancient skull encrusted with moss and bits of bark, with golden teeth.
      The skull was discovered in the heart of a tree during a logging operation near what turned out to be an Aztec temple.
    </para>

    <para><emphasis role="bold">Abilities: </emphasis>
    Each night the coniferous skull is in your possession, it gives you +4 to Concentration checks during the following day.
    However, it causes -10 to Wisdom checks during the night.
    </para>
  </section>

  <section id="artefact-dagger-of-kom-el-hisn">
    <title>Dagger of Kom el-Hisn</title>

    <para>
      A ceremonial dagger covered in hieroglyphics.
      If translated, it may be recognized as spell 1141 from the <emphasis>Book of the Two Ways</emphasis>, reading:
    </para>

    <para>
      I am the torch and the flame, I am the spear which is in the hand which is stabbed at those who are below.
      O you of fire, beware of me, for I am the knife which pierces the middle of your head.
      It is I who repels  the earth-gods, and the river Styx.
      Ra is my protector, and blesses my visage.
      Do not challenge Amun-Ra.
    </para>

    <para><emphasis role="bold">Abilities: </emphasis>
    Prick your finger, or otherwise draw your own blood with the dagger, and it grants you +4 to your next attack roll.
    You suffer 2 HP damage each time you cut yourself.
    </para>
  </section>

  <section id="artefacts-kagu-tsuchi-Kiseru">
    <title>Kagu-tsuchi's kiseru</title>

    <para>
      An ornate and elegant pipe made of metal and bamboo, said to have belonged to a holy man studying Kagu-tsuchi, the god of fire.
      Stolen from a temple in 1893, it was thought to have been lost until it turned up at an 10 years later.
      The pipe was purchased by a historian, and has since been passed around the private collector market.
    </para>

    <para><emphasis role="bold">Abilities: </emphasis>
    Grants immunity to fire to its bearer.
    </para>
  </section>
  
  <section id="artefacts-white-lens">
    <title>White Lens</title>

    <para>
      The White Lens is an opaque circle of glass 4 feet in diameter, contained in a frame of brass.
      The frame was added sometime in the 18th century, but the origins of the glass are unknown.
    </para>

    <para>
      The light of other days shine through the lens.
      Put it against a window, and images of the past are illuminated in the room beyond.
      Adjusting the lens alters how far back the lens goes; generally, the closer the focal point, the shorter the time distortion.
      Focussing the lens on a point right in front of it might only go back a few hours; beam it across the length of a large room, and you look back centuries.
    </para>

    <para>
      The lens is currently embedded in the wall of a private library, where the owner uses the light to read books that have long since decayed past the point of legibility.
    </para>

    <para><emphasis role="bold">Abilities:</emphasis>
    The lens can indeed create images of past times.
    The beam from the lens is limited in size, and it can illuminate an area only a few feet across as any time, but its abilities are otherwise infallible.
    Adjusting the lens to "tune into" a particular time requires a Knowledge (physics) check (DC varies, depending on the level of precision required).
    </para>

    <para>
      The true danger of the White Lens is that it leaks.
      Things from other days flicker at the edges of the lens, and sometimes they are rumoured to crawl through.
    </para>
    </section>
  </chapter>
  
