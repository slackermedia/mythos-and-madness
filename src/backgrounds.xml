<chapter id="backgrounds">
  <title>Backgrounds</title>

  <para>
    All player characters have an background.
    You may acquire additional jobs as the game progresses, but you only gain benefits of your starting background.
    Benefits include:
  </para>

  <itemizedlist>
    <listitem>
      <para>
	Your background unlocks 3 skills, as listed in the description of the background you choose.
	For each skill you take, fill in the bubble to the left of the skill on the character sheet.
	If a skill bubble is already filled, add 1 point to that skill instead.
      </para>
    </listitem>
    <listitem>
      <para>
	Some backgrounds grant you a bonus feat.
	Review the feats in the <xref linkend="feats" /> chapter, and write the feat and its page number in the FEATS box on your character sheet.
      </para>
    </listitem>
    <listitem>
      <para>
	Some backgrounds also provide a <emphasis role="bold">Reputation</emphasis> bonus.
	Enter your reputation in the REPUTATION box on your character sheet.
      </para>
    </listitem>
    <listitem>
      <para>
	Your background also provides a <emphasis role="bold">Wealth</emphasis> bonus.
	Enter your wealth bonus in the WEALTH box on your character sheet.
      </para>
    </listitem>
  </itemizedlist>
  
  <section id="backgrounds-academic">
    <title>Academic</title>

    <para>
      Academia is the industry of higher education.
      This includes not only universities, but archaeologists, antiquarians, scholars, professors, teachers, and so on.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 23+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 3) Technology, Craft (writing), Decipher Script, Gather Information, Knowledge (occult lore, art, behavioural sciences, business, civics, current events, earth and life sciences, history, physical sciences, popular culture, tactics, technology, or theology and philosophy), Research, or add a new Read/Write Language or a new Speak Language.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +2
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> A journal, typewriter or computer, mobile phone or address book with several academic contacts, a pocket knife, and a compass or pocket watch.
	</para>
      </listitem>      
    </itemizedlist>
  </section>

  <section id="backgrounds-adventurer">
    <title>Adventurer</title>

    <para>
      Tomb raiders, explorers, daredevils, relic hunters, extreme sport enthusiasts, field scientists, thrill-seekers, investigative journalists, and others.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 15+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 2) Bluff, Climb, Demolitions, Disable Device, Drive, Escape Artist, Intimidate, Jump, Knowledge (occult lore, streetwise, tactics or technology), Move Silently, Pilot, Ride, Spot, Survival, Swim, Treat Injury or add a new Speak Language.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Feats:</emphasis> (Choose 1) Archaic Weapons Proficiency, Brawl or Personal Firearms Proficiency.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Backpack, camping kit with rations and basic supplies, flashlight, a GPS or collection of maps, a mobile device or address book, 50 ft. of rope, contacts who can provide a variety of modes of transportation at a low (and sometimes no) cost.
	</para>
      </listitem>      
    </itemizedlist>
  </section>  

  <section id="backgrounds-athlete">
    <title>Athlete</title>

    <para>
      Amateur athletes of all types, professional athletes, including gymnasts, wrestlers, boxers, martial artists, swimmers, skaters, and other sports.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Str or Dex 13+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 3) Balance, Climb, Drive, Jump, Ride, Swim, or Tumble.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Feats:</emphasis> (Choose 1) Acrobatic, Archaic Weapons Proficiency, Brawl, Endurance, or Focussed.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +2
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> A mobile device or address book, contacts in the local sports industry, a civilian automobile.
	</para>
      </listitem>      
    </itemizedlist>
  </section>  

  <section id="backgrounds-blue">
    <title>Blue collar</title>

    <para>
      Factory workers, food service jobs, construction, service industry jobs, taxi drivers, postal workers, newsies, delivery services, and other jobs not considered to be desk jobs.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 18+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 3) Craft (electronic, mechanical, or structural), Climb, Drive, Handle Animal, Intimidate, Repair, or Ride.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +0
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +2
	</para>
      </listitem>
      <listitem>
	<para>
	<emphasis role="bold">Gear:</emphasis> Backpack, an old laptop or a tattered notebook and pen, a personalized autographed photo of a national celebrity, a mobile device or address book with some work friends, a pocket knife or multitool or small set of useful but mundane tools.</para>
	<para>
	  You may choose 1 kit appropriate to your job (electrician's kit, car repair kit, lawn care kit, paint kit, and so on).
	  Not all kits are listed in the <xref linkend="gear" /> chapter, but you and your GM can create one together by agreeing on what tools are appropriate.
	</para>
      </listitem>            
    </itemizedlist>
  </section>  

  <section id="backgrounds-celebrity">
    <title>Celebrity</title>

    <para>
      Anyone in the spotlight or the public gaze, including actors, singers, comedians, entertainers of all types, newscasters, and radio and television personalities.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 15+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 3) Bluff, Craft (visual art or writing), Diplomacy, Disguise or Perform (act, dance, keyboards, percussion instruments, sing, stand-up, stringed instruments, or wind instruments).
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +4
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +4
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or journal, mobile device or address book with contacts in the entertainment biz, a fine gift from a fan or patron, a tattered copy of a screenplay or sheet music or manuscript, a disguise kit or access to a variety of costumes at a moment's notice.
	</para>
      </listitem>            
    </itemizedlist>
  </section>  

  <section id="backgrounds-creative">
    <title>Creative</title>

    <para>
      Fine artists, including illustrators, copywriters, cartoonists, graphic artists, novelists, magazine columnists, actors, sculptors, game designers, musicians, screenwriters, photographers, roleplaying game writers, graphic designers, and so on.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 15+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 3) Bluff, Technology, Craft (visual art or writing), Disguise, Forgery, Knowledge (occult lore or art), Perform (act, dance, keyboards, percussion instruments, sing, stand-up, stringed instruments or wind instruments) or Spot.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +2
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +2
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Tools of your trade, a laptop or typewriter, a mobile device or address book with contact info for other creative workers, a napkin with a phone number (but no name) scribbled on it, comp tickets for an upcoming show, a prop from a recent performance.
	</para>
      </listitem>            
    </itemizedlist>
  </section>

  <section id="backgrounds-criminal">
    <title>Criminal</title>

    <para>
      Con artists, burglars, thieves, crime family soldiers, gang members, bank robbers, and other types of career criminals.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 15+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 2) Bluff, Disable Device, Disguise,
Forgery, Gamble, Hide, Knowledge (streetwise), Move Silently, or Sleight of Hand.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Feats:</emphasis> (Choose 1) Brawl, Deceptive, or Personal Firearms Proficiency.
	</para>
      </listitem>      
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +0
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> A mobile device or address book with local criminal contacts, lockpicks, an expensive-looking (actually counterfeit) timepiece, a single key, somebody else's wallet and ID (not modified to your identity).
	</para>
      </listitem>                  
    </itemizedlist>
  </section>

  <section id="backgrounds-dilettante">
    <title>Dilettante</title>

    <para>
      Dilettantes get their wealth from their family or from a trust.
      The typical dilettante has few responsibilities, much less a job, but may find a hobby or obsession or passion to fill spare time.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 18+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 1) Gamble, Intimidate, Knowledge (current events or popular culture), Ride or add a new Speak Language.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +5
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or typewriter, mobile device or address book with wealthy contacts, an expensive item of jewelry or an expensive timepiece, a sports car or a boat or similar.
	</para>
      </listitem>                  
    </itemizedlist>
  </section>

  <section id="backgrounds-doctor">
    <title>Doctor</title>

    <para>
      A physician (general practitioner or specialist), surgeon, psychiatrist, veterinarian.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 25+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 2) Craft (pharmaceutical), Technology,
Handle Animal, Knowledge (behavioral sciences, earth and life sciences, or technology), Search or Treat Injury.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +3
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> First aid kit, laptop or notebook, a mobile device or address book with contact information for scientists or doctors, access to medical supplies and pharmaceuticals, a recent academic paper from a peer-reviewed journal.
	</para>
      </listitem>                  
    </itemizedlist>
  </section>

  <section id="backgrounds-ems">
    <title>Emergency services</title>

    <para>
      Rescue workers,  emergency medical technicians, firefighters, paramedics, and hazmat handlers.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 18+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 2) Balance, Climb, Technology, Drive,
Jump, Knowledge (behavioral sciences, earth and life sciences, or technology), Search, Treat Injury, Swim.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +2
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> First aid kit, pocket knife or multitool, a laptop or notebook, a mobile device or address book with contact information of people you've helped in the past, flashlight.
	</para>
      </listitem>
    </itemizedlist>
  </section>
  
  <section id="backgrounds-entrepreneur">
    <title>Entrepreneur</title>

    <para>
      Business owners, capitalists, salespeople, or anyone who runs their own business as their own boss.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 18+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 2) Bluff, Diplomacy, Gamble, or Knowledge (business, current events or technology).
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +4
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or notebook, a mobile device or address book with contact information for frequent investors, an expensive ink pen, the first dollar bill you ever earned, a small metal pillbox you're holding for a friend and cannot open.
	</para>
      </listitem>      
    </itemizedlist>
  </section>

<section id="backgrounds-investigator">
    <title>Investigator</title>

    <para>
      Private investigators, researchers, investigative journalists, photojournalists, police detectives, criminologists, criminal profilers, government agents, corporate agents, and anyone who uses their unique set of skills to dig deep, gather evidence, and analyse clues.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 23+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 2) Technology, Craft (visual art or writing), Decipher Script, Forgery, Gather Information, Investigate, Knowledge (behavioural sciences, civics, earth and life sciences, or streetwise), Research, Search, or Sense Motive.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Bonus feat:</emphasis> (Choose 1) Brawl, Meticulous, or Personal Firearms Proficiency.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +0
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +2
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or notebook, a mobile device or an address book containing past client contact info or the numbers of a few police and criminals, a Derringer or similar handgun and a holster, a camera.
	</para>
      </listitem>      
    </itemizedlist>
  </section>

<section id="backgrounds-law">
    <title>Law enforcement</title>

    <para>
      Uniformed police, state troopers, federal police, federal agents, corporate security, special forces, military police, security guards.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 20+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 2) Diplomacy, Drive, Gather Information, Intimidate, Knowledge (civics, earth and life sciences, streetwise or tactics) or Listen.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Bonus feat:</emphasis> (Choose 1) Combat Martial Arts, Light Armour Proficiency or Personal Firearms Proficiency.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +2
	</para>
      </listitem>
            <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or notebook, a mobile device or an address book containing contact info of local business owners you've helped in the past, a Glock, flashlight, metal baton or club, handcuffs, an empty cigarette lighter.
	</para>
      </listitem>
    </itemizedlist>
  </section>

  <section id="backgrounds-military">
    <title>Military</title>

    <para>
      Army, navy, air force, and so on.
      This includes members of groups not recognised by any nation, such as freedom fighters.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 18+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 2) Climb, Demolitions, Drive, Hide, Knowledge (tactics), Move Silently, Navigate, Pilot, Survival, or Swim.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Bonus feat:</emphasis> (Choose 1) Brawl, Combat Martial Arts, Light Armour Proficiency or Personal Firearms Proficiency.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +0
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Dogtags, a mobile device or an address book with the contact info for members of your previous squadron, GPS or collection of maps, pocket knife, Glock or Derringer or similar handgun, a car or utility vehicle.
	</para>
      </listitem>      
    </itemizedlist>
  </section>

  <section id="backgrounds-religious">
    <title>Religious</title>

    <para>
      Ordained clergy of all persuasions, priests, priestesses, theological scholars, and experts on religious studies.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 23+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 3) Decipher Script, Knowledge (occult lore, art, behavioural sciences, history, streetwise, or theology and philosophy), Listen or Sense Motive.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +2
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or typewriter, a mobile device or address book with contact info for other members of your order, a holy book, a holy trinket, a map given to you by a dying friend. 
	</para>
      </listitem>
    </itemizedlist>
  </section>

  <section id="backgrounds-rural">
    <title>Rural</title>

    <para>
      Farm workers, hunters, arborists, and anyone who lives in works in a rural setting.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 15+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 2) Balance, Climb, Drive, Handle Animal, Repair, Ride, Survival or Swim.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Bonus feat:</emphasis> (Choose 1) Brawl, Guide, or Personal Firearms Proficiency
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +0
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or notebook, a mobile device or address book with contact info of local seasonal workers, pocket knife, a metal part to an unknown machine, utility vehicle.
	</para>
      </listitem>      
    </itemizedlist>
  </section>

  <section id="backgrounds-student">
    <title>Student</title>

    <para>
      A student is anyone enroled in an institution of higher learning.
      This includes secondary or high school (college), university, technical institute, graduate school, and religious or military schools.
      A college-age student should also pick a major field of study.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 15+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 3) Technology, Knowledge (any, except appraisal, streetwise or tactics), Perform (act, dance, keyboards, percussion instruments, sing, stand-up, stringed instruments or wind instruments) or Research.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +0
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or notebook, a mobile device or address book with contact info for your instructors, a rare book, pocket watch or similar timepiece that's sometimes 10 minutes fast and other times 10 minutes slow, a collection of sweatshirts or jackets from several different educational institutions.
	</para>
      </listitem>      
    </itemizedlist>
  </section>

  <section id="backgrounds-technician">
    <title>Technician</title>

    <para>
      Scientists and engineers.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 23+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 3) Technology, Craft (chemical, electronic, mechanical or structural), Knowledge (business, earth and life sciences, physical sciences or technology), Repair, Research.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +0
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +3
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or notebook, mobile device or address book listing colleagues in your field, a basic kit with supplies to test your work, a cargo van.
	</para>
      </listitem>      
    </itemizedlist>
  </section>

  <section id="backgrounds-white">
    <title>White collar</title>

    <para>
      Office workers, accountants, insurance agents, bank staff, investors, financial advisors, clerks, real estate agents, and the whole spectrum of upper- and mid-management.
    </para>

    <itemizedlist>
      <listitem>
	<para>
	  <emphasis role="bold">Prerequisite:</emphasis> Age 23+
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Skills:</emphasis> (Choose 3) Technology, Technology, Diplomacy, Knowledge (art, business, civics, earth and life sciences, history, physical sciences or technology), Research.
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Reputation:</emphasis> +1
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Wealth:</emphasis> +3
	</para>
      </listitem>
      <listitem>
	<para>
	  <emphasis role="bold">Gear:</emphasis> Laptop or notebook, a mobile device or address book with contact information of your competitors, a matchbook from an establishment you don't recall ever visiting, a pocket watch or timepiece, a sedan or similar civilian automobile.
	</para>
      </listitem>      
    </itemizedlist>
  </section>
</chapter>
