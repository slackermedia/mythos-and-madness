<chapter id="fear">
  <title>Horror Saves</title>

  <para>
    A Horror Save is a special kind of Will saving throw that occurs when you are confronted by a horrific or disturbing encounter.
    Upon a failed saving throw, you are scared, nauseated, or otherwise hindered.
    Upon success, you can act relatively normally.
    Your courage is enough to overcome his fear.
    Horror Saves are more damaging that normal saving throws; if you fail by a large margin, you may become psychologically scarred.
    There are three kinds of Horror Saves:
  </para>

  <itemizedlist>
    <listitem><para>
    <emphasis role="bold">Panic Saves</emphasis> occur when you are in immediate danger, such as a burning building, onrushing car, or rampaging psychopath. Relatively mundane situations can trigger panic saves, but they are also the least damaging of the three types of horror.
    </para></listitem>

    <listitem><para>
      <emphasis role="bold">Fear Saves</emphasis> occur when you are in a disturbing situation.
      There is no immediate danger, just a brooding, lurking, waiting darkness.
      It is the fear of the haunted house, of the shuttered room, of the noise in the attic.
      It is the fear of strangers, of shadows, of nameless fears, and night terrors.
    </para></listitem>

    <listitem><para>
      <emphasis role="bold">Madness Saves</emphasis> occur when you are confronted by the impossible and the inexplicable, when all the rules of reality crumble away or are revealed as lies.
      They are the rarest of the three Horror saves, as they only happen when you are directly exposed to the supernatural (although truly bizarre situations can also trigger a Madness Save, such as discovering everyone you know are actually NSA agents and you have been living in a fake town constructed inside a psychological warfare research laboratory for the last 30 years).
      Failing a Madness save always has dire consequences.
    </para></listitem>
  </itemizedlist>

  <section id="fear-shock">
    <title>Shock Points</title>

    <para>
      As you fail Horror Saves, you accumulate Shock Points.
      These points measure how shaken you are by your experiences.
      These points penalise your Horror Saves (-1 to all Horror saves for each Shock point).
      Shock points can become long-term phobias or other psychological traumas over time (see <xref linkend="fear-reduce-shock"/>).
    </para>

    <para>
      When you gain a Shock Point, roll a d100 on this <xref linkend="fear-flaws"/> table for an instant temporary flaw, which endures until the end of the adventure or until you take action to <xref linkend="fear-reduce-shock"/>.
    </para>

    <orderedlist id="fear-flaws">
    <title>Flaws (d100)</title>
    <!-- 0-10 --> 
    <listitem><para>Take no prisoners.</para></listitem>
    <listitem><para>Minor paralysis. You lose precision in your dominant hand. Take -2 penalty to your Dexterity score.</para></listitem>
    <listitem><para>You reliably forget people's names.</para></listitem>
    <listitem><para>You are uncomfortable around the colour green.</para></listitem>
    <listitem><para>You are severely judgemental of others.</para></listitem>
    <listitem><para>You are distrustful of one randomly selected PC.</para></listitem>
    <listitem><para>All you want in life is a perfect sushi roll.</para></listitem>
    <listitem><para>You just can't stop preaching about one of your bonds.</para></listitem>
    <listitem><para>You're jealous of anyone doing anything you have skill ranks in.</para></listitem>
    <listitem><para>Gambling addiction. You compulsively play the lotto and take -1 penalty to your Wealth.</para></listitem>
    <!-- 11-20 -->
    <listitem><para>No room for caution.</para></listitem>
    <listitem><para>You believe something in your past is a dark secret others are trying to discover.</para></listitem>
    <listitem><para>You must win any debate, regardless of how it may damage your relationships with others.</para></listitem>
    <listitem><para>You blamed someone for a crime, even though you weren't sure they did it. You feel guilty about this now.</para></listitem>
    <listitem><para>You are uncomfortable around the colour red.</para></listitem>
    <listitem><para>You view any difference of opinion as a slander to you, and you make note of each one so you can get back at those who insult you.</para></listitem>
    <listitem><para>Roll 1d20. This number is your unlucky number.</para></listitem>
    <listitem><para>You are afraid of heights. Take -4 penalty on all Climbing checks.</para></listitem>
    <listitem><para>You feel people can tell when you're lying. Take -4 on Bluff checks.</para></listitem>
    <listitem><para>You have developed a nervous tick when telling a lie. Roll your Bluff checks twice and take the lower of the two.</para></listitem>
    <!-- 21-30 -->
    <listitem><para>You believe you're invincible.</para></listitem>
    <listitem><para>You're hearing voices.</para></listitem>
    <listitem><para>You check the horoscopes compulsively for guidance.</para></listitem>
    <listitem><para>You can't concentrate on anything for longer than a few minutes.</para></listitem>
    <listitem><para>You blindly trust the judgement of one randomly selected PC.</para></listitem>
    <listitem><para>You think you've lost something in your equipment list. Even after you find it, you feel something is missing.</para></listitem>
    <listitem><para>You can't keep a secret.</para></listitem>
    <listitem><para>You're afraid to spend money.</para></listitem>
    <listitem><para>Chaos always results in a net benefit.</para></listitem>
    <listitem><para>You believe you're being followed.</para></listitem>
    <!-- 31-40 -->
    <listitem><para>You compulsively collect random items you find, believing they'll be useful some day.</para></listitem>
    <listitem><para>You won't take orders from anyone.</para></listitem>
    <listitem><para>You're sarcastic about everything.</para></listitem>
    <listitem><para>If you're outnumbered, you retreat from a confrontation.</para></listitem>
    <listitem><para>You speak loudly, regardless of the situation.</para></listitem>
    <listitem><para>Only the strong survive.</para></listitem>
    <listitem><para>You feel that all of your friends are making you do all the work.</para></listitem>
    <listitem><para>You have lost all tact.</para></listitem>
    <listitem><para>You believe you've developed a powerful body odour. You must conceal it in any way possible.</para></listitem>
    <listitem><para>You have a hard time reading and writing. This causes -4 on relevant skill checks.</para></listitem>
    <!-- 41-50 -->
    <listitem><para>You speak only in the third person ("we" and "us" instead of "I").</para></listitem>
    <listitem><para>You are always optimistic.</para></listitem>
    <listitem><para>You are always pessimistic.</para></listitem>
    <listitem><para>You believe everyone you meet owes you money.</para></listitem>
    <listitem><para>You can't make a decision.</para></listitem>
    <listitem><para>You can only make a decision by flipping a coin.</para></listitem>
    <listitem><para>You doubt your abilities and must be cajoled into using your skills. Before making a skill check, roll a d100. If you roll 50 or lower, you automatically fail the check.</para></listitem>
    <listitem><para>You suddenly remember that there's a warrant out for your arrest for something you did not do.</para></listitem>
    <listitem><para>You are always right.</para></listitem>
    <listitem><para>You have almost no pain tolerance.</para></listitem>
    <!-- 51-60 -->
    <listitem><para>You flatter everyone.</para></listitem>
    <listitem><para>You lie about almost everything, even when there's no reason to lie.</para></listitem>
    <listitem><para>You lie about literally everything. Roll a d100 before relaying information to other people. If you roll 50 or lower, you lie.</para></listitem>
    <listitem><para>You have suddenly found religion, and can't stop trying to recruit others.</para></listitem>
    <listitem><para>You have suddenly found religion, and you believe everything that happens is the result of an angry god punishing you for lack of faith.</para></listitem>
    <listitem><para>You protect your friends, even if they don't need protecting.</para></listitem>
    <listitem><para>Your eyes are fatigued, and you're mentally exhausted. Take -2 to ranged attack rolls and -4 on Decipher Script checks.</para></listitem>
    <listitem><para>You are given to laughter at inappropriate moments.</para></listitem>
    <listitem><para>You resent not being the center of attention.</para></listitem>
    <listitem><para>You are angry with your parents, and blame them for your misfortune. When you experience misfortune, you compulsively explain this to everyone around you.</para></listitem>
    <!-- 61-70 -->
    <listitem><para>You are obsessed with calculating the odds of everything that could happen, or has happened. Your calculations are rarely correct, but you calculate them anyway.</para></listitem>
    <listitem><para>You misuse big words in an attempt to sound more intelligent.</para></listitem>
    <listitem><para>You often lose your train of thought, even in the middle of a sentence.</para></listitem>
    <listitem><para>You believe there's an elaborate conspiracy working against everything you do.</para></listitem>
    <listitem><para>You believe everything that happens to you is part of a complex cosmic plan.</para></listitem>
    <listitem><para>You have no sense of direction, even when faced with a signpost showing you the way.</para></listitem>
    <listitem><para>It's not stealing if you need it more than someone else.</para></listitem>
    <listitem><para>You refuse to defend yourself, whether someone is debating you or physically attacking you.</para></listitem>
    <listitem><para>You believe you are loved by everyone, and that everything you do is for the common good.</para></listitem>
    <listitem><para>You are easily distracted, and often get lost in thought. Take -4 to all Concentration checks.</para></listitem>
    <!-- 71-80 -->
    <listitem><para>You can't resist shiny things.</para></listitem>
    <listitem><para>You are frightfully bad with technology, often breaking any high-tech device you touch. Take -4 penalty to all Technology checks..</para></listitem>
    <listitem><para>In stressful situations, you have a 50% chance of falling asleep.</para></listitem>
    <listitem><para>You are easily disheartened.</para></listitem>
    <listitem><para>You have an imaginary friend.</para></listitem>
    <listitem><para>Compared to you, everybody is <emphasis>just so stupid</emphasis>, and they all need to be reminded of it constantly.</para></listitem>
    <listitem><para>You feel the need to give away your possessions to those in need.</para></listitem>
    <listitem><para>You are clumsy and imprecise. Take -4 to all Craft checks.</para></listitem>
    <listitem><para>You have lost all emotion. You are numb to the world.</para></listitem>
    <listitem><para>Your demeanour suffers from recent frights. Take -4 to all Diplomacy checks.</para></listitem>
    <!-- 81-90 -->
    <listitem><para>You use your own slang words in the belief that they'll catch on.</para></listitem>
    <listitem><para>You're nervous due to a recent fright. You're ill at ease, and you take -4 on Disable Device checks.</para></listitem>
    <listitem><para>You see omens and portents in everything.</para></listitem>
    <listitem><para>You have a poor sense of balance. Take -4 penalty to all Balance checks.</para></listitem>
    <listitem><para>You are uncomfortable in serious situations and make jokes or quirky remarks to lighten the mood.</para></listitem>
    <listitem><para>You're extremely lazy.</para></listitem>
    <listitem><para>You believe you can speak to animals.</para></listitem>
    <listitem><para>You're a hopeless matchmaker, and see a blossoming love between any two people.</para></listitem>
    <listitem><para>You are given to crying.</para></listitem>
    <listitem><para>You compulsively wash your hands and change clothes as frequently as possible.</para></listitem>
    <!-- 91-00 -->
    <listitem><para>You are overly polite and formal.</para></listitem>
    <listitem><para>You can't speak to anyone without first critiquing them in some way.</para></listitem>
    <listitem><para>You feel the need to bribe everyone, no matter what they're doing. You compulsively give away money, and insist upon doing so even when it's refused or illegal. Take -2 penalty to all CHA and Wealth checks.</para></listitem>
    <listitem><para>You love talking to people about your background or profession.</para></listitem>    
    <listitem><para>You're extremely nosey.</para></listitem>
    <listitem><para>At your first chance to do so, you get a tattoo in a conspicuous location (face, neck, hand).</para></listitem>
    <listitem><para>You want everyone to be miserable, whether physically, mentally, or emotionally.</para></listitem>
    <listitem><para>You're able to find common ground with even your fiercest foe.</para></listitem>
    <listitem><para>You obey the law no matter what. When coerced or persuaded by friends to break the law, you're overcome with guilt and report yourself to the authorities the first chance you get.</para></listitem>
    <listitem><para>You believe you're a fictional character in a board game, controlled by the whims of a careless player and random dice throws.</para></listitem>
  </orderedlist>    
    
  </section>
  
  <section id="fear-horror-save">
    <title>Making Horror Saves</title>

    <para>
      A Horror save is made just like a normal Will save: roll 1d20 and add your Will save bonus.
      Success is a total equal to or higher than the Difficulty Class (DC).
      Three of the four basic character classes have a penalty to a particular kind of Horror Save.
    </para>

    <para>
      Your roll may be affected by who else is around:
    </para>
    
    <itemizedlist>
      <title>Horror Save modifiers</title>
      <listitem><para>Alone (no allies): -4</para></listitem>
      <listitem><para>One other person: -2</para></listitem>
      <listitem><para>Small group (2-6 people): +0</para></listitem>
      <listitem><para>A crowd: +2</para></listitem>
      <listitem><para>More enemies than friends present: -2</para></listitem>
    </itemizedlist>

    <para>
      Your save roll can also be modified by lighting conditions (assuming you're aware of the source of the Horror Save without sufficient light):
    </para>
    
    <itemizedlist>
      <title>Modifiers due to lighting</title>
      <listitem><para>Dark/Dim Light: -2</para></listitem>
      <listitem><para>Total Darkness: -4</para></listitem>
    </itemizedlist>
    
    <para>
       A character's sanity level can also influence the efficacy of your Horror Save:
    </para>

    <itemizedlist>
      <title>Insanity modifier</title>
      <listitem><para>Each Shock Point: -1</para></listitem>
      <listitem><para>Mild Phobia to stimulus: -2</para></listitem>
      <listitem><para>Severe Phobia to stimulus: -4</para></listitem>
    </itemizedlist>
  </section>

  <section id="fear-panic">
    <title>Panic Saves</title>

    <para>
      A Panic Save is required when you are suddenly confronted by immediate danger.
      The danger does not have to be supernatural, but must be a threat <emphasis>right now</emphasis>.
      Immediacy is the essence of panic.
    </para>

    <itemizedlist>
      <title>Sample Panic Save DC</title>
      <listitem><para>You hear a gunshot: DC 5</para></listitem>
      <listitem><para>A car drives straight towards you: DC 10</para></listitem>
      <listitem><para>A figure suddenly looms out of a dark alleyway: DC 10</para></listitem>
      <listitem><para>You take 10 or more points of damage in one round: DC 10</para></listitem>
      <listitem><para>You're suddenly attacked by a knife-wielding maniac: DC 12</para></listitem>
      <listitem><para>You're trapped in a burning building: DC 12</para></listitem>
      <listitem><para>Someone shoots at you: DC 12</para></listitem>
      <listitem><para>A large explosion happens near you: DC 15</para></listitem>
      <listitem><para>You find yourself caught in a crossfire: DC 15</para></listitem>
      <listitem><para>Chased by a hideous monster: DC 20</para></listitem>
      <listitem><para>Chased through a dark maze by an other-worldly being: DC 25</para></listitem>
    </itemizedlist>

    <para>
      If you fail the Panic Save by 5 or less, he freezes for a number of rounds equal to the margin of failure.
      If you fail by 6 or more, you either flee or fight.
      If you roll a 1 on your Panic Save, you also gain 1d3 Shock points.
    </para>

    <itemizedlist>
      <listitem><para>
	<emphasis role="bold">Freezing:</emphasis>
	When you freeze in the face of panic, you cannot move or act.
	You are flatfooted when defending yourself.
	You automatically unfreezes if struck, injured, or even touched by anything.
	You may make a Will save (DC equal to the Panic save DC -5) to unfreeze if someone shouts at you or otherwise tries to snap you out of your momentary stupor.
	When you unfreeze, you may roll initiative and act normally thereafter.
    </para>
      </listitem>

      <listitem>
	<para>
	  <emphasis role="bold">Fight or Flight:</emphasis>
	  When you suffer a flight or fight response, you have a 50% chance of panicking and a 50% chance of attacking or otherwise doing something in response to whatever caused the panic.
	  You may make a Fortitude save (DC equal to the Panic Save) to take the opposite reaction.
	</para>

	<para>
	  When fleeing, you move as fast as possible away from the thing or event that caused you to panic.
	  If unable to flee, you freeze, as above.
	  You panic for a number of rounds equal to the margin of failure.
	  You can defend yourself as normal, but you cannot attack.
    </para>
      </listitem>

      <listitem>
	<para>
	  When fighting, you must either attack the thing that caused you to panic, or else pick some goal (finding survivors, retrieving an item lost or damaged in the explosion) and obsessively try to accomplish that goal.
	  You become single-mindedly fixated on the attack or goal, and are incapable of thinking about anything else.
	  This state lasts for a number of rounds equal to the margin of failure.
	</para>
      </listitem>
    </itemizedlist>
  </section>

  <section id="fear-save">
    <title>Fear Saves</title>

    <para>
      Fear Saves are required when you are in a frightening or disturbing situation.
    </para>

    <itemizedlist>
      <title>Sample Fear Saves DC</title>
      <listitem><para>Wandering through a dark and empty house: DC 5</para></listitem>
      <listitem><para>Remembering a nightmare: DC 5</para></listitem>
      <listitem><para>Finding a dead animal: DC 10</para></listitem>
      <listitem><para>Spooky and unexplained sounds: DC 10</para></listitem>
      <listitem><para>Seeing truly disturbing or disgusting images: DC 12</para></listitem>
      <listitem><para>Lost in a dark maze: DC 12</para></listitem>
      <listitem><para>Trapped in a nightmare and unable to wake up: DC 12</para></listitem>
      <listitem><para>Finding a corpse: DC 15</para></listitem>
      <listitem><para>Being deliberately confronted with the object of your phobia: DC 15</para></listitem>
      <listitem><para>Finding a friend’s corpse: DC 20</para></listitem>
      <listitem><para>Seeing a ghost: DC 20</para></listitem>
    </itemizedlist>

    <para>
      If you fail your Fear Save by 5 or less, you are shaken for a number of rounds equal to the margin of failure.
      If you fail by 6 or more, you are repulsed.
      If you fail by 10 or more, you are repulsed and gain 1d4 Shock points.
    </para>

    <para>
      <emphasis role="bold">Shaken:</emphasis>
      When you are shaken, you take a -2 penalty to attack rolls, skill checks, and saving throws (including Horror Saves).
    </para>

    <para>
      <emphasis role="bold">Repulsion:</emphasis>
      For 1d4 rounds, you are repulsed by fear and are affected by one of three conditions, of your choice:
    </para>

    <itemizedlist>
      <listitem><para><emphasis role="bold">Nauseated:</emphasis>
      You cannot attack or do anything except move.
      </para></listitem>
      <listitem><para><emphasis role="bold">Fascinated:</emphasis>
      You cannot do anything except stare in horror at whatever scared you.
      You are considered stunned for the duration.      
      </para></listitem>
      <listitem><para><emphasis role="bold">In denial:</emphasis>
      You are in denial momentarily, refusing to accept that whatever just happened actually happened.
      Unless forced to acknowledge it (for example, if you're attacked by the monster you are otherwise denying), you act as if whatever scared you does not exist.
      If you are forced to recognise the threat again, you must make another Fear save.
      </para></listitem>
    </itemizedlist>

    <para>
      After this, you are shaken for a number of minutes equal to the margin of failure.
      You suffer an additional -2 penalty on Horror saves while shaken.
    </para>
  </section>

  <section id="fear-madness">
    <title>Madness Saves</title>

    <para>
      <emphasis role="bold">Madness Saves</emphasis> are required when the walls of reality shatter.
      When the utterly impossible and horrific manifests in front of you, a Madness save is required.
    </para>

    <itemizedlist>
      <title>Sample Madness Saves</title>
      <listitem><para>Reading the diary of a madman: DC 10</para></listitem>
      <listitem><para>Repeated synchronicities (for instance, deja vu): DC 12</para></listitem>
      <listitem><para>Having an out-of-body experience: DC 12</para></listitem>
      <listitem><para>Finding evidence of a paradox: DC 15</para></listitem>
      <listitem><para>Seeing proof that 2+2=5: DC 15</para></listitem>
      <listitem><para>Meeting an alternate-reality or time-travelling version of yourself: DC 20</para></listitem>
      <listitem><para>Experiencing a paradox or dimensional fold: DC 25</para></listitem>
    </itemizedlist>

    <para>
      If you fail a Madness Save by 5 or less, you are shaken for a number of rounds equal to the margin of failure.
      If you fail by 6 or more, you gains 1d6 Shock points and become temporarily insane.
    </para>
    
    <itemizedlist>
      <listitem><para><emphasis role="bold">Shaken:</emphasis>
      A shaken character takes a -2 penalty to attack rolls, skill checks, and saving throws.
    </para></listitem>
    <listitem><para><emphasis role="bold">Temporary Insanity:</emphasis>
    A character driven temporarily insane by an event rolls 1d6 on this table:
  </para>
  <itemizedlist>
    <listitem><para>1-3: cower for d6 rounds, probably screaming and crying</para></listitem>
    <listitem><para>4-5: suffer a breakdown, robbing you of the ability to process any information at all for the duration. You are considered paralaysed</para></listitem>
    <listitem><para>6: you faint and remain unconscious for d6 rounds</para></listitem>
  </itemizedlist>
    </listitem>
    </itemizedlist>
    
    <para>
      All conditions last for 1d6 rounds, and the character is considered shaken for a number of minutes equal to the margin of failure and suffers an additional -2 penalty on Horror saves while shaken.
    </para>

    <bridgehead>Multiple Simultaneous Horror Saves</bridgehead>

    <para>
      If an entity or event has two or three different Horror Saves associated with it, you only make one roll and use it for all applicable the saves, applying the appropriate modifiers and comparing to each individual DC.
      A hideous monster bursting up out of the floor at you, for example, may cause both Panic and Fear.
      Instead of rolling twice, roll only one save, applying your Panic bonus for the Panic check, and any Fear bonus for the Fear check.
  </para>  
  </section>

  <section id="fear-reduce-shock">
    <title>Reduce shock points</title>

    <para>
      Shock points measure short-term damage to your sanity.
      They are gained by failing Horror Saves.
      For every Shock point you have, you suffer -1 morale penalty to all Horror Saves.
    </para>

    <para>
      You can take action to relieve the effects of shock.
    </para>

  <itemizedlist id="fear-reduce-shock-relief">
      <title>Temporary Relief</title>

      <listitem><para><emphasis role="bold">Sheer Grit:</emphasis>
      You can remove 1 point of Shock by making a Will save (DC 20) once the duration of whatever effect caused the Shock has expired.
      For example, if you gained a Shock point while being panicked for 10 rounds, you can make a Will save at the end of the 10 rounds.
      If multiple Shock points are gained during a single encounter, you can only remove 1 Shock point by making the Will save.
      </para></listitem>

      <listitem><para><emphasis role="bold">Distractions:</emphasis>
      You can also remove Shock points by turning to alcohol, drugs, or some other distraction.
      If you block the memory of whatever caused the Shock in this fashion, you may remove up to 4 Shock points.
      Impairment from drugs or alcohol lasts 2 hours per Shock point, and causes a -1 circumstance penalty per Shock point removed to all attack rolls and skill checks for the duration of the impairment.
      Furthermore, when it comes to buying off any remaining Shock points (see Long-term Effects, below), the cost of any related additions is reduced by 1.
      For example, if you drink yourself into a stupor to remove 3 Shock points, the cost for mild or severe Alcoholism is reduced by 1 point.
    </para></listitem>

    <listitem><para><emphasis role="bold">Deal With This Later:</emphasis>
    Finally, you can remove the penalties to Horror saves from accumulated Shock points by repressing the trauma to be dealt with later.
    Choosing to Deal With This Later is a full-round action.
    You gain 1 extra Shock point and must still buy off all Shock points at some point, but you can ignore the penalty from any Shock.
    If you gain more Shock points after declaring that you'll Deal With This Later, that Shock causes the usual penalties.
    You can choose to Deal With This Later again, but you gain 1 extra Shock point each time you do.
  </para>

  <para>
    For example, Bob the reporter has 5 Shock points as a result of exploring a haunted house.
    This gives him a rather crippling -5 penalty to all Horror saves.
    His camerawoman Alice is still trapped somewhere inside the house.
    To enter the building, Bob must make a Fear save, but the penalty from Shock means that he has almost no chance of making the save.
    He chooses to Deal With This Later.
    He gains another Shock point (bringing him to a rather worrying total of 6), but he now has no penalties to his Horror saves.
    He heads into the house, gets mauled by more ghouls, and eventually manages to exorcise and rescue Alice.
    In the course of these events, Bob gains another 4 Shock points.
    At the end of the adventure, he has a -4 penalty to his Horror saves (from the last 4 Shock points), and a total of 10 Shock points.
      And because it's the end of the adventure, he has to deal with them all.
    </para>
    </listitem>

    <listitem><para><emphasis role="bold">Take a Breather:</emphasis>
      This can only be taken with GM approval, and is primarily intended for extremely horrific games, or one-shot games or particularly short campaigns in which characters aren't expected to get the chance to deal with accumulated Shock.
    </para>

    <para>
      Characters may choose to take a breather when they are temporarily safe from harm.
      During this time, they can deal with Shock points so they can continue on with the adventure.
    </para>
    </listitem>
  </itemizedlist>
  </section>

  <section id="fear-shock-other-uses">
    <title>Other Uses for Shock Points</title>

    <para>
      <emphasis role="bold">Facing Your Fears:</emphasis>
      If you have already encountered a particular horror, you may declare that you're facing your fear before encountering it again.
      Choosing to Face Your Fears is a full-round action.
      You gain 1 Shock point, but also gain a +10 morale bonus to the first Horror check caused by the monster or situation.
    </para>
  
    <para>
      <emphasis role="bold">Inner Reserves:</emphasis>
      If you have had an ability score drained or damaged, you may take 1 Shock point to ignore the penalty for 1 round.
    </para>
  </section>

  <section id="fear-long-term-effects">
    <title>Long-term Effects</title>

    <para>
      Shock points never last for long.
      Whenever one of the following events occurs, you must deal with all your accumulated Shock points.
      These "checkpoints" are:
    </para>

    <itemizedlist>
      <listitem><para>The end of the current adventure, as determined by the GM.</para></listitem>
      <listitem><para>You gain a level.</para></listitem>
      <listitem><para>There is downtime in the adventure of at least two weeks (in game time).</para></listitem>
    </itemizedlist>

    <para>
      When one of these events occurs, you must deal with your Shock points.
      Firstly, your mind has a certain amount of resilience.
      You may make a Will save; if this save is successful, you may remove the first Shock points.
      The more damage your mind has taken, the harder it is to shrug off.
    </para>

    <itemizedlist>
      <title>Natural Recovery DC</title>
      <listitem><para>1-3: DC 10</para></listitem>
      <listitem><para>4-6: DC 15</para></listitem>
      <listitem><para>7-10: DC 20</para></listitem>
      <listitem><para>11 or more: DC 25</para></listitem>
    </itemizedlist>

    <para>
      If you are treated by a psychiatrist, the psychiatrist may make a Knowledge (behavioural science) check at the same DC as the Will save.
      A successful check reduces the DC for the your Will save by 2.
    </para>

    <para>
      As part of a reward for completing a scenario, the GM may remove Shock points.
      
      If you successfully protected or aided your Bonds, or if you eradicated a major threat or source of weirdness, the GM may choose to remove 1d6 Shock Points as a reward.
    </para>
  </section>
  
  <section id="fear-shock-disorder">
    <title>Remaining Shock Points</title>

    <para>
      If any Shock points remain, you accumulate psychological problems and disorders (you essentialy "purchase" disorders with Shock Points).
      When you reach a checkpoint, you must spend all your Shock points.
      A character can only purchase each disorder once.
      Going from a disorder marked <emphasis>mild</emphasis> to a disorder marked <emphasis>severe</emphasis> only costs 2 points.
      The exception is Toughening: you may take the Toughening disorder once every time you buy off
accumulated Shock points.
    </para>

    <para>
      If by some disaster, you have every disorder on the Results of Shock list and still have Shock points left over, you may buy off the remaining Shock points at the cost of one Wisdom point (your <emphasis>score</emphasis>, which effects your Wisdom modifier) for each Shock point.
      A character with a Wisdom score of 0 is irretrievably insane.
    </para>

    <itemizedlist>
      <title>Results of Shock</title>
      <listitem><para>Addiction, mild: 2 points</para></listitem>
      <listitem><para>Addiction, severe: 4 points</para></listitem>
      <listitem><para>Amnesia: 4 points</para></listitem>
      <listitem><para>Depression: 2 points</para></listitem>
      <listitem><para>Disassociative Identity Disorder: 4 points</para></listitem>
      <listitem><para>Obsession: 2 points</para></listitem>
      <listitem><para>Obsessive-Compulsive Disorder: 2 points</para></listitem>
      <listitem><para>Paranoia: 2 points</para></listitem>
      <listitem><para>Phobia, mild: 2 points</para></listitem>
      <listitem><para>Phobia, severe: 4 points</para></listitem>
      <listitem><para>Sociopathy: 2 points</para></listitem>
      <listitem><para>Schizophrenia: 2 points</para></listitem>
      <listitem><para>Toughening: 1 points</para></listitem>
    </itemizedlist>
  </section>

  <section id="fear-disorder-remove">
    <title>Removing Disorders</title>

    <para>
      Should you  gain a level as a Pinko after character creation, you can remove any 1 disorder with a rating of 2 or less on the Results of Shock Table, with a Will save (DC 15).
    </para>

    <para>
      Prolonged psychiatric care of at least 3 months, with no odd occurrences during the therapy, allows you to make a Will save (DC 13 + the disorder's rating) to remove a single disorder.
      The psychiatrist may make a Knowledge (behavioural science) check at the same DC as the Will save.
      A successful check reduces the DC for your Will save by 2 points.
    </para>

    <section id="fear-disorder-list">
      <title>Disorders</title>

      <itemizedlist>
	<listitem><para><emphasis role="bold">Addiction, mild:</emphasis>
	You need a fix regularly, and can only go a maximum number of days equal to your Charisma bonus + your Constitution bonus before entering withdrawal. Your Wealth bonus is reduced by 2 and you suffer a –4 penalty while under the affects of your chosen poison. When in withdrawal, you must make a Fortitude save each day to be able to function. If your save fails, you are considered shaken until a new fix is gained and suffer periods of nausea.
	</para></listitem>
	
	  <listitem><para><emphasis role="bold">Addiction, severe:</emphasis>
	  As mild addiction, but you need a daily dose of whatever you're addicted to. You can go a maximum number of days equal to your Charisma or Constitution bonus (whichever is higher) before entering withdrawal. Your Wealth bonus falls by 2 per month of addiction.
	  </para></listitem>

	  <listitem><para><emphasis role="bold">Amnesia:</emphasis> You block out the memories of whatever caused your trauma. You must make a Will save (DC 20) to recall any memories. You automatically gain 5 Shock points as the repressed memories flood back if you encounter the source of your amnesia again.
	  </para></listitem>

	  <listitem><para><emphasis role="bold">Depression:</emphasis> You lose hope for the future, or you cease to attach emotional importance to certain aspects of life. You suffer a -2 morale penalty to all Will saves and must make a Will save (DC 10) to motivate himself each day, otherwise you stay in his home. If dragged out, you suffer a -2 penalty to all attack rolls and skill checks. Oddly, the morale penalty does not apply to Horror checks. You simply do not care enough to be particularly bothered.</para></listitem>

	  <listitem><para><emphasis role="bold">Disassociative Identity Disorder:</emphasis> Your psyche fragments to create an alternate personality to deal with your trauma. Eventually, you may develop multiple personalities which come to the fore in times of stress (you switches personality whenever you fail a Horror save). The alternate personalities have the game statistics and abilities as the primary, but each has different a distinctly different mindset. Additionally, each additional personality will slowly accumulate disorders of its own (as the GM decrees), quite probably swamping the primary personality eventually.</para></listitem>

	  <listitem><para><emphasis role="bold">Obsession:</emphasis> You become obsessed with something. Roll 1d6 for each Bond. Upon rolling a 1, take a Bond point away from another Bond and assign it to the one you're rolling for. You become obsessed with that Bond before all others. If you do not roll a 1 at all, the GM chooses a Bond to assign a Bond point to, or invents a new Bond to replace an existing Bond.
	  </para></listitem>

	  <listitem><para><emphasis role="bold">Obsessive-Compulsive Disorder:</emphasis> You develop a set of rituals and nervous responses you must perform on a regular interval. This unsettles people around you, causing a -2 penalty to all your Charisma-based checks. If you're prevented from performing your ritual, you suffer a -2 penalty to all skill checks until it can be performed, or for the next 1d6 hours.
	  </para></listitem>

	  <listitem><para><emphasis role="bold">Paranoia:</emphasis> You become convinced that you have enemies everywhere. You suffer a -4 penalty on Sense Motive checks. Upon a failed Sense Motive check, you always believe that whomever you're interacting with is plotting against you.</para></listitem>

	  <listitem><para><emphasis role="bold">Phobia, mild:</emphasis> You have an unnatural fear of some phenomena, and you suffer a -2 to any Horror checks involving it.</para></listitem>

	  <listitem><para><emphasis role="bold">Phobia, severe:</emphasis> You have an overwhelming fear of some phenomena and suffers a -4 to any Horror checks involving it.
	  </para></listitem>
	  
	  <listitem><para><emphasis role="bold">Sociopathy:</emphasis>
	  A dangerous disability, you become sociopathic, slowly losing the emotional capacity to connect with anything you experience or are involved in. You degrade 1 permanent Bond by 1 point. If this leaves the Bond with no points left devoted to it, then it no longer exists as a Bond. If you have no Bonds left at all, then you are an incurable sociopath and you're taken over by the GM.
	  </para></listitem>
	  
	  <listitem><para><emphasis role="bold">Schizophrenia:</emphasis> You begin to suffer from hallucinations and delusions. If you fail any Horror check, the margin of failure is doubled: if you fail a roll by 4, then you work out the results as if you had missed the DC by 8.</para></listitem>

	  <listitem><para><emphasis role="bold">Toughening:</emphasis> You become more resistant to horror. Increase your Horror save bonus for 1 type of Horror (Panic, Fear, or Madness) by 1.
	  </para></listitem>
      </itemizedlist>
  </section>
  </section>
</chapter>
