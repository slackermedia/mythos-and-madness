<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import
      href="../docbook/fo/docbook.xsl"/>

  <!--xsl:import
      href="/usr/share/xml/docbook/xsl-stylesheets-1.78.1/fo/docbook.xsl"/-->
  <xsl:param name="variablelist.as.blocks" select="1"></xsl:param>
  <xsl:param name="orderedlist.label.width">2.5em</xsl:param>
  <xsl:param name="ulink.show">0</xsl:param>  
  <xsl:param name="page.margin.inner">0.5in</xsl:param>
  <xsl:param name="page.margin.outer">0.5in</xsl:param>
  <xsl:param name="page.margin.top">0.45in</xsl:param>
  <xsl:param name="page.margin.bottom">0.5in</xsl:param>
  <xsl:param name="title.margin.left">0</xsl:param>
  <xsl:param name="title.start.indent">0</xsl:param>
  <xsl:param name="body.start.indent">0</xsl:param>

  <xsl:template match="title" mode="chapter.titlepage.recto.auto.mode">  
    <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	      xsl:use-attribute-sets="chapter.titlepage.recto.style" 
	      margin-left="{$title.margin.left}"
	      color="#448822"
	      font-size="21pt"
	      font-weight="bold"
	      padding-bottom="12pt"
	      font-family="{$title.font.family}">
      <xsl:call-template name="component.title">
	<xsl:with-param name="node" select="ancestor-or-self::chapter[1]"/>
      </xsl:call-template>
    </fo:block>
  </xsl:template>

<xsl:template match="emphasis[@role='strong']">
    <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	      font-weight="bold">
    <xsl:apply-templates/>
  </fo:block>
</xsl:template>

<!-- title page logo -->

<xsl:attribute-set name="informalfigure.properties">
  <xsl:attribute name="text-align">center</xsl:attribute>
  <xsl:attribute name="padding-top">2em</xsl:attribute>
</xsl:attribute-set>

<!-- admonitions, breakout boxes -->

<xsl:template match="tip">
  <xsl:variable name="id">
    <xsl:call-template name="object.id"/>
  </xsl:variable>
  <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
	    space-before.minimum="0.8em"
            space-before.optimum="1em"
            space-before.maximum="1.2em"
            start-indent="0.25in"
            end-indent="0.25in"
	    padding-top="6pt"
	    padding-bottom="2pt"
	    padding-left="4pt"
	    padding-right="4pt">
    <xsl:if test="$admon.textlabel != 0 or title">
      <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
		keep-with-next='always'
		xsl:use-attribute-sets="admonition.title.properties"
		font-family="Junction"
		color="#448822"
		font-weight="bold">
         <xsl:apply-templates select="." mode="object.title.markup"/>
      </fo:block>
    </xsl:if>

    <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
	      xsl:use-attribute-sets="admonition.properties"
	      font-family="Junction">
      <xsl:apply-templates/>
    </fo:block> 
  </fo:block>
</xsl:template>

<xsl:template match="note">
  <xsl:variable name="id">
    <xsl:call-template name="object.id"/>
  </xsl:variable>

  <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
	    space-before.minimum="0.8em"
            space-before.optimum="1em"
            space-before.maximum="1.2em"
            start-indent="0.25in"
            end-indent="0.25in"
	    background-color="#ffffff"
            border="0.5pt solid black"
	    margin-left="0pt"
	    margin-right="0pt"
	    padding-top="6pt"
	    padding-bottom="2pt"
	    padding-left="4pt"
	    padding-right="4pt">
    <xsl:if test="$admon.textlabel != 0 or title">
      <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
		keep-with-next='always'
		xsl:use-attribute-sets="admonition.title.properties">
         <xsl:apply-templates select="." mode="object.title.markup"/>
      </fo:block>
    </xsl:if>

    <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
	      xsl:use-attribute-sets="admonition.properties">
      <xsl:apply-templates/>
    </fo:block>
  </fo:block>
</xsl:template>

<xsl:template match="important">
  <xsl:variable name="id">
    <xsl:call-template name="object.id"/>
  </xsl:variable>
  <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
	    space-before.minimum="0.8em"
            space-before.optimum="1em"
            space-before.maximum="1.2em"
            start-indent="0.25in"
            end-indent="0.25in"
	    background-color="#d9fad9"
	    border-top="1pt solid black"
	    border-bottom="1pt solid black"
	    border-left="0pt solid black"
	    border-right="0pt solid black"
	    margin-left="0pt"
	    margin-right="0pt"
	    padding-top="6pt"
	    padding-bottom="2pt"
	    padding-left="4pt"
	    padding-right="4pt"
	    font-family="Junction">
    <xsl:if test="$admon.textlabel != 0 or title">
      <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
		keep-with-next='always'
		xsl:use-attribute-sets="admonition.title.properties"
		font-family="Junction"
		color="#000000"
		font-weight="bold">
         <xsl:apply-templates select="." mode="object.title.markup"/>
      </fo:block>
    </xsl:if>
    <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
	      xsl:use-attribute-sets="admonition.properties"
	      font-family="Junction">
      <xsl:apply-templates/>
    </fo:block> 
  </fo:block>
</xsl:template>

<!-- page break -->

<xsl:template match="processing-instruction('hard-pagebreak')">
  <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format"
	    break-after='page'/>  
</xsl:template>

<!-- image span -->

<xsl:attribute-set name="pgwide.properties">
  <xsl:attribute name="span">all</xsl:attribute>
  <xsl:attribute name="padding-top">0pt</xsl:attribute>
  <xsl:attribute name="padding-bottom">0pt</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="component.titlepage.properties">
  <xsl:attribute name="span">all</xsl:attribute>
</xsl:attribute-set>

<!-- footer -->

<xsl:template name="redist.text">
    <xsl:choose>
      <xsl:when test="$redist.text = 'ogl'">
	<xsl:text>All content licensed Open Game License 1.0a</xsl:text>  
      </xsl:when>

      <xsl:when test="$redist.text = 'nc'">
	<xsl:text>Non-product identity content licensed Creative Commons BY-NC</xsl:text>  
      </xsl:when>

      <xsl:when test="$redist.text = 'ncnd'">
	<xsl:text>Non-product identity content licensed Creative Commons BY-NC-ND</xsl:text>  
      </xsl:when>

      <xsl:when test="$redist.text = 'nd'">
	<xsl:text>Non-product identity content licensed Creative Commons BY-ND</xsl:text>  
      </xsl:when>

      <xsl:when test="$redist.text = 'no'">
	<xsl:text>Not for resale. Permission granted to print or photocopy this document for personal use only.</xsl:text>  
      </xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:attribute-set name="footer.table.properties">
  <xsl:attribute name="padding-left">5pt</xsl:attribute>
  <xsl:attribute name="font-family">Junction</xsl:attribute>
  <xsl:attribute name="padding-right">5pt</xsl:attribute>
  <xsl:attribute name="font-size">8</xsl:attribute>
  <xsl:attribute name="font-weight">italics</xsl:attribute>
</xsl:attribute-set>


<xsl:template name="footer.content">  
  <xsl:param name="pageclass" select="''"/>
  <xsl:param name="sequence" select="''"/>
  <xsl:param name="position" select="''"/>
  <xsl:param name="gentext-key" select="''"/>

  <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format">  
    <!-- sequence can be odd, even, first, blank -->
    <!-- position can be left, center, right -->
    <xsl:choose>

      <xsl:when test="$sequence = 'odd' and $position = 'left'">  
        <xsl:call-template name="redist.text"/>  
      </xsl:when>

      <xsl:when test="$sequence = 'odd' and $position = 'center'">
      </xsl:when>

      <xsl:when test="$sequence = 'odd' and $position = 'right'">
        <fo:page-number/>  
      </xsl:when>

      <xsl:when test="$sequence = 'even' and $position = 'left'">  
        <fo:page-number/>
      </xsl:when>

      <xsl:when test="$sequence = 'even' and $position = 'center'">
      </xsl:when>

      <xsl:when test="$sequence = 'even' and $position = 'right'">
        <xsl:call-template name="redist.text"/>  
      </xsl:when>

      <xsl:when test="$sequence = 'first' and $position = 'left'"> 
        <xsl:call-template name="redist.text"/>  
      </xsl:when>

      <xsl:when test="$sequence = 'first' and $position = 'right'">  
        <fo:page-number/>
      </xsl:when>

      <xsl:when test="$sequence = 'first' and $position = 'center'"> 
      </xsl:when>

      <xsl:when test="$sequence = 'blank' and $position = 'left'">
        <fo:page-number/>
      </xsl:when>

      <xsl:when test="$sequence = 'blank' and $position = 'center'">
        <xsl:text>This page intentionally left blank</xsl:text>  
      </xsl:when>

      <xsl:when test="$sequence = 'blank' and $position = 'right'">
      </xsl:when>

    </xsl:choose>
  </fo:block>
</xsl:template>

</xsl:stylesheet>
