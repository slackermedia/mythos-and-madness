while read -r line; do
    echo "<varlistentry><term>"`echo $line | cut -d";" -f1`"</term>"
    echo '<listitem><para><emphasis role="bold">Crew:</emphasis>'
    echo `echo $line | cut -d";" -f2 | cut -d" " -f2`"</para>"
    echo '<para><emphasis role="bold">Passengers:</emphasis>'`echo $line | cut -d";" -f2 | cut -d' ' -f3`'</para>'
    echo '<para><emphasis role="bold">Cargo:</emphasis>'`echo $line | cut -d";" -f2 | cut -d' ' -f4-5`'</para>'
    echo '<para><emphasis role="bold">Init:</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f6`'</para>'

    echo '<para><emphasis role="bold">Manoeuvre:</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f7`'</para>'
    echo '<para><emphasis role="bold">Top speed:</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f8`'</para>'
    echo '<para><emphasis role="bold">Chase scale:</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f9 | tr -cd '[[:digit:]]'`'</para>'
    echo '<para><emphasis role="bold">Defence:</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f10`'</para>'
    echo '<para><emphasis role="bold">Hardness:</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f11`'</para>'
    echo '<para><emphasis role="bold">Hit points (HP):</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f12`'</para>'
    echo '<para><emphasis role="bold">Size:</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f13`'</para>'
    echo '<para><emphasis role="bold">Purchase DC:</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f14`'</para>'
    echo '<para><emphasis role="bold">Restriction:</emphasis>'`echo $line  | cut -d";" -f2 | cut -d' ' -f15-`'</para>'    
    echo '</listitem></varlistentry>'
done < weapon.tmp
